

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.function.BiFunction;

public class BSTSimple<Key extends Comparable<Key>, Value> implements BSTdetail<Key, Value> {
    @Override
    public Boolean contains(Key key) {
        return get(key) != null;
    }

    @Override
    public void putAll(Map<Key, Value> map) {
        // CONSIDER optionally randomize the input
        for (Map.Entry<Key, Value> entry : map.entrySet()) put(entry.getKey(), entry.getValue());
    }

    @Override
    public int size() {
        return root != null ? root.count : 0;
    }

    @Override
    public void inOrderTraverse(BiFunction<Key, Value, Void> f) {
        doTraverse(0, root, f);
    }

    @Override
    public Value get(Key key) {
        return get(root, key);
    }

    @Override
    public Value put(Key key, Value value) {
        NodeValue nodeValue = put(root, key, value);
        if (root == null) root = nodeValue.node;
        if (nodeValue.value==null) root.count++;
        return nodeValue.value;
    }

    @Override
    public void deleteMin() {
        root = deleteMin(root);
    }

    @Override
    public Set<Key> keySet() {
        return null;
    }

    public BSTSimple() {
    }

    public BSTSimple(Map<Key, Value> map) {
        this();
        putAll(map);
    }

    Node root = null;
    
    @Override
    public void delete(Key key) {
        // TODO- Implement this delete method or add your variations of delete.
    	root = delete(root,key);
    }

    private Value get(Node node, Key key) {
        if (node == null) return null;
        int cf = key.compareTo(node.key);
        if (cf < 0) return get(node.smaller, key);
        else if (cf > 0) return get(node.larger, key);
        else return node.value;
    }

    /**
     * Method to put the key/value pair into the subtree whose root is node.
     *
     * @param node  the root of a subtree
     * @param key   the key to insert
     * @param value the value to associate with the key
     * @return a tuple of Node and Value: Node is the
     */
    private NodeValue put(Node node, Key key, Value value) {
        // If node is null, then we return the newly constructed Node, and value=null
        if (node == null) return new NodeValue(new Node(key, value), null);
        int cf = key.compareTo(node.key);
        if (cf == 0) {
            // If keys match, then we return the node and its value
            NodeValue result = new NodeValue(node, node.value);
            node.value = value;
            return result;
        } else if (cf < 0) {
            // if key is less than node's key, we recursively invoke put in the smaller subtree
            NodeValue result = put(node.smaller, key, value);
            if (node.smaller == null)
                node.smaller = result.node;
            if (result.value==null)
                result.node.count++;
            return result;
        } else {
            // if key is greater than node's key, we recursively invoke put in the larger subtree
            NodeValue result = put(node.larger, key, value);
            if (node.larger == null)
                node.larger = result.node;
            if (result.value==null)
                result.node.count++;
            return result;
        }
    }

    private Node delete(Node x, Key key) {
        // TO IMPLEMENT
        if (x == null) return null;
        int cmp = key.compareTo(x.key);
        if (cmp < 0) x.smaller = delete(x.smaller, key);
        else if (cmp > 0) x.larger = delete(x.larger, key);
        else {
            if (x.larger == null) return x.smaller;
            if (x.smaller == null) return x.larger;

            Node t = x;
            x = min(t.larger);
            x.larger = deleteMin(t.larger);
            x.smaller = t.smaller;
        }
        x.count = size(x.smaller) + size(x.larger) + 1;
        return x;
    }

    private Node deleteMin(Node x) {
        if (x.smaller == null) return x.larger;
        x.smaller = deleteMin(x.smaller);
        x.count = 1 + size(x.smaller) + size(x.larger);
        return x;
    }

    private int size(Node x) {
        return x == null ? 0 : x.count;
    }
    
    private Node search(Key key){
    	Node currentNode = root;
    	if(currentNode == null) return null;
    	else{
    	while(currentNode!=null && key!=currentNode.key){
    		int cmp = key.compareTo(currentNode.key);
    		if(cmp > 0) currentNode = currentNode.larger;
    		else currentNode = currentNode.smaller;
    	}
    	}
    	return currentNode;
    }
    
    private int getMaxDepth(Node x){
    	if(x==null) return 0;
    	int left = getMaxDepth(x.smaller);
    	int right = getMaxDepth(x.larger);
    	return 1+ Math.max(left,right);
    }
    
    @Override
    public int getMaxDepth(){
    	return getMaxDepth(root);
    }

    private Node min(Node x) {
        if (x == null) throw new RuntimeException("min not implemented for null");
        else if (x.smaller == null) return x;
        else return min(x.smaller);
    }

    /**
     * Do a generic traverse of the binary tree starting with node
     * @param q determines when the function f is invoked ( lt 0: pre, ==0: in, gt 0: post)
     * @param node the node
     * @param f the function to be invoked
     */
    private void doTraverse(int q, Node node, BiFunction<Key, Value, Void> f) {
        if (node == null) return;
        if (q<0) f.apply(node.key, node.value);
        doTraverse(q, node.smaller, f);
        if (q==0) f.apply(node.key, node.value);
        doTraverse(q, node.larger, f);
        if (q>0) f.apply(node.key, node.value);
    }

    private class NodeValue {
        private final Node node;
        private final Value value;

        NodeValue(Node node, Value value) {
            this.node = node;
            this.value = value;
        }

        @Override
        public String toString() {
            return node + "<->" + value;
        }
    }

    class Node {
        Node(Key key, Value value) {
            this.key = key;
            this.value = value;
        }

        final Key key;
        Value value;
        Node smaller = null;
        Node larger = null;
        int count = 0;

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder("Node: " + key + ":" + value);
            if (smaller != null) sb.append(", smaller: " + smaller.key);
            if (larger != null) sb.append(", larger: " + larger.key);
            return sb.toString();
        }

    }

    private Node makeNode(Key key, Value value) {
        return new Node(key, value);
    }

    private Node getRoot() {
        return root;
    }

    private void setRoot(Node node) {
        if(root==null){
            root = node;
            root.count++;
        }else
            root = node;
    }
    private void show(Node node, StringBuffer sb, int indent) {
        if (node == null) return;
        for (int i = 0; i < indent; i++) sb.append("  ");
        sb.append(node.key);
        sb.append(": ");
        sb.append(node.value);
        sb.append("\n");
        if (node.smaller != null) {
            for (int i = 0; i <= indent; i++) sb.append("  ");
            sb.append("smaller: ");
            show(node.smaller, sb, indent + 1);
        }
        if (node.larger != null) {
            for (int i = 0; i <= indent; i++) sb.append("  ");
            sb.append("larger: ");
            show(node.larger, sb, indent + 1);
        }
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        show(root, sb, 0);
        return sb.toString();
    }
    
    public static void main(String[] args){
    	int size = 500;//origin size of the bst
    	int keybound = 1000;//keybound
    	Map<Integer,Integer> map = new HashMap<>();
    	for(int i=0;i<size;i++){
    		map.put((int)(Math.random()*keybound),1);
    	}
    	BSTdetail<Integer,Integer> bst = new BSTSimple<>(map);
    	//above is seeding a bst;
    	int N;//the number of operations;
    	//for 1000 times, do the operation
    	ArrayList<Integer> gaps = new ArrayList<>();//initialize an arraylist to restore the gap between maxdepth^2 and size N;
    	for(N=100;N<=10000;N+=100){
    		int countput = 0,countdelete = 0;
        	for(int j=0;j<N;j++){
        		Random rand = new Random();//decide on which operation to run
        		int chosenkey = (int)(Math.random()*keybound);//decide on which key to operate
        		if(rand.nextBoolean()){
        			bst.put(chosenkey, 1);
        			countput++;
        		}else{
        			bst.delete(chosenkey);
        			countdelete++;
        		}
        	}
        	int maxdepth = bst.getMaxDepth();//the max depth of the tree
        	System.out.println(bst.size()+" operation count:delete: "+countdelete+" ,put: "+countput+" The maxdepth of the tree after "+N+" operations"+" is "+maxdepth);
        	gaps.add(maxdepth*maxdepth-bst.size());
    	}
    	try{
        		FileOutputStream fis = new FileOutputStream("./src/result.csv");
        		OutputStreamWriter isr = new OutputStreamWriter(fis);
        		BufferedWriter bw = new BufferedWriter(isr);
        		int n = 100;
        		for(int k:gaps){
        			String content = n + "," + k + "\n";
        			n+=100;
        			bw.write(content);
        			bw.flush();
        		}
        		bw.close();
        	} catch(IOException e) {
        		e.printStackTrace();
        	}
    }
}
